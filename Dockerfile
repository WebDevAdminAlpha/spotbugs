FROM golang:1.15 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .

# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM alpine:3.12

ARG ANT_VERSION
ARG FINDSECBUGS_VERSION
ARG GLIBC_VERSION=2.32-r0
ARG ZLIB_VERSION=1:1.2.11-4-x86_64
ARG ZLIB_SHA1SUM=be23c6422981570d2656623d4d5b0ab57703a1ed
ARG GCC_LIBS_VERSION=10.2.0-4-x86_64
ARG GCC_LIBS_SHA1SUM=02d952d238df1f2f5b0c10a168039213eaec695c

ENV FINDSECBUGS_VERSION ${FINDSECBUGS_VERSION:-1.11.0}

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-4.2.3}

ENV ASDF_VERSION "v0.8.0-rc1"
ENV ASDF_DATA_DIR="/opt/asdf"

ENV HOME=/root
WORKDIR $HOME

RUN apk add --update --no-cache bash

COPY config /root
COPY spotbugs /spotbugs

RUN bash /root/install.sh

# Install analyzer
COPY --from=build --chown=root:root /go/src/app/analyzer /analyzer-binary

ENTRYPOINT []
ADD start.sh /analyzer
RUN chmod +x /analyzer

RUN \
    touch /root/.bash_profile; \
    chmod -R g+rwx /root

CMD ["/analyzer", "run"]
