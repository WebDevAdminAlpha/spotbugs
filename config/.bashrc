#!/bin/bash -l

update_java_home() {
  local java_path
  java_path="$(asdf which java)"
  if [[ -n "${java_path}" ]]; then
    export JAVA_HOME
   JAVA_HOME="$(dirname "$(dirname "$(realpath "${java_path}")")")"
  fi
}

function switch_to_exact() {
  local tool=$1
  local version=$2

  asdf global "$tool" "$version"
  if [[ "$tool" = "java" ]]; then
    update_java_home
  fi
}

function switch_to() {
  local tool=$1
  local major_version=$2
  local version
  version="$(grep "$tool" "/root/.tool-versions"| tr ' ' '\n' | grep "adoptopenjdk-$major_version.")"

  switch_to_exact "$tool" "$version"
}

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/asdf.sh"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/java/set-java-home.bash
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/ant/set-ant-home.bash